# AttributedServiceRegistration

Register IServiceCollection services by tagging them with attributes rather than manually registering each service individually.

Add to your project with `dotnet add package AttributedServiceRegistration --version 1.0.0`

Provides `RegisterAttributedServices()` as an extension method to IServiceCollection.

Supports;

- Classes with no interface inheritance.
- Classes that inherit from an interface (providing the interface is named the same as the implementation class, prefixed with an 'I').

- Singleton, Scoped, and Transient lifetimes.
- Optional Service Key can be provided to register the service as a Keyed service.
- Either register services in the calling assembly, or provide a list of assemblies to the `RegisterAttributedServices` method.

Example;

```csharp
public interface IMySingletonService
{
  ...
}

[SingletonService]
public class MySingletonService : IMySingletonService
{
  ...
}

...

[ScopedService(12345)]
public class MyKeyedScopedService
{
  ...
}

[TransientService]
public class MyTransientService
{
  ...
}

...

myServices.RegisterAttributedServices();
```

Would be equivalent to;

```csharp
myServices.AddSingleton<IMySingletonService, MySingletonService>();
myServices.AddKeyedScoped<MyKeyedScopedService>(12345);
myServices.AddTransient<MyTransientService>();
```

