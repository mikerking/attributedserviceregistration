using AttributedServiceRegistration.Attributes;

namespace AttributedServiceRegistration.Tests;

public interface IMySingletonService
{
}

/// <summary>
/// Singleton service which inherits IMySingletonService
/// </summary>
[SingletonService]
public class MySingletonService : IMySingletonService
{
}

/// <summary>
/// Singleton service with a service key
/// </summary>
[SingletonService("MyService")]
public class MyKeyedSingletonService
{
}

/// <summary>
/// Scoped service
/// </summary>
[ScopedService]
public class MyScopedService
{
}

/// <summary>
/// Scoped service with service key
/// </summary>
[ScopedService("AnotherService")]
public class MyKeyedScopedService
{
}

/// <summary>
/// Transient service
/// </summary>
[TransientService]
public class MyTransientService
{
}

/// <summary>
/// Transient service with service key
/// </summary>
[TransientService(1234)]
public class MyKeyedTransientService
{
}
