using AttributedServiceRegistration.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Tests;

public class Tests
{
    private IServiceCollection _serviceCollection;

    /// <summary>
    /// Setup and register attributed services
    /// </summary>
    [SetUp]
    public void Setup()
    {
        _serviceCollection = new ServiceCollection();
        _serviceCollection.RegisterAttributedServices();
    }

    /// <summary>
    /// Ensure singleton service is registered, referenced by interface
    /// </summary>
    [Test]
    public void SingletonServicesRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(IMySingletonService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Singleton));
        Assert.That(service.ServiceKey, Is.Null);
    }

    /// <summary>
    /// Ensure singleton service with a service key is registered
    /// </summary>
    [Test]
    public void KeyedSingletonServicesRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(MyKeyedSingletonService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Singleton));
        Assert.That(service.ServiceKey, Is.EqualTo("MyService"));
    }

    /// <summary>
    /// Ensure scoped service is registered
    /// </summary>
    [Test]
    public void ScopedServiceRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(MyScopedService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Scoped));
        Assert.That(service.ServiceKey, Is.Null);
    }

    /// <summary>
    /// Ensure scoped service with service key is registered
    /// </summary>
    [Test]
    public void KeyedScopeServiceRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(MyKeyedScopedService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Scoped));
        Assert.That(service.ServiceKey, Is.EqualTo("AnotherService"));
    }

    /// <summary>
    /// Ensure transient service is registered
    /// </summary>
    [Test]
    public void TransientServiceRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(MyTransientService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Transient));
        Assert.That(service.ServiceKey, Is.Null);
    }

    /// <summary>
    /// Ensure transient service with service key is registered
    /// </summary>
    [Test]
    public void KeyedTransientServiceRegistered()
    {
        var service =  _serviceCollection.FirstOrDefault(
            service => service.ServiceType == typeof(MyKeyedTransientService));

        Assert.That(service, Is.Not.Null);
        Assert.That(service.Lifetime, Is.EqualTo(ServiceLifetime.Transient));
        Assert.That(service.ServiceKey, Is.EqualTo(1234));
    }
}
