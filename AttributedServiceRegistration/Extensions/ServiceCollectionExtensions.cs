using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AttributedServiceRegistration.Attributes;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Register attributed services in only the calling assembly
        /// </summary>
        /// <param name="serviceCollection">Instance of IServiceCollection</param>
        public static void RegisterAttributedServices(this IServiceCollection serviceCollection)
        {
            RegisterAttributedServices(serviceCollection, new[] { Assembly.GetCallingAssembly() });
        }

        /// <summary>
        /// Register attributed services in the provided collection of assemblies
        /// </summary>
        /// <param name="serviceCollection">Instance of IServiceCollection</param>
        /// <param name="assemblies">A list of assemblies to examine for services</param>
        public static void RegisterAttributedServices(this IServiceCollection serviceCollection,
            IEnumerable<Assembly> assemblies)
        {
            // Collect all attributed types
            var implementationTypes = assemblies
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.IsDefined(typeof(ServiceBaseAttribute)) && !type.IsInterface);

            foreach (var implementationType in implementationTypes)
            {
                var serviceType = implementationType.GetInterface("I" + implementationType.Name);
                var attribute = implementationType.GetCustomAttribute<ServiceBaseAttribute>();

                if (attribute is null)
                    continue;

                switch (attribute.Lifetime)
                {
                    // Service is a Singleton
                    case ServiceLifetime.Singleton:
                        if (attribute.ServiceKey is null)
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddSingleton(serviceType: implementationType);
                            }
                            else
                            {
                                serviceCollection.AddSingleton(
                                    serviceType: serviceType,
                                    implementationType: implementationType);
                            }
                        }
                        else
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddKeyedSingleton(
                                    serviceType: implementationType,
                                    serviceKey: attribute.ServiceKey);
                            }
                            else
                            {
                                serviceCollection.AddKeyedSingleton(
                                    serviceType: serviceType,
                                    serviceKey: attribute.ServiceKey,
                                    implementationType: implementationType);
                            }
                        }

                        break;

                    // Service is Scoped
                    case ServiceLifetime.Scoped:
                        if (attribute.ServiceKey is null)
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddScoped(serviceType: implementationType);
                            }
                            else
                            {
                                serviceCollection.AddScoped(
                                    serviceType: serviceType,
                                    implementationType: implementationType);
                            }
                        }
                        else
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddKeyedScoped(
                                    serviceType: implementationType,
                                    serviceKey: attribute.ServiceKey);
                            }
                            else
                            {
                                serviceCollection.AddKeyedScoped(
                                    serviceType: serviceType,
                                    serviceKey: attribute.ServiceKey,
                                    implementationType: implementationType);
                            }
                        }

                        break;

                    // Service is Transient
                    case ServiceLifetime.Transient:
                        if (attribute.ServiceKey is null)
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddTransient(serviceType: implementationType);
                            }
                            else
                            {
                                serviceCollection.AddTransient(
                                    serviceType: serviceType,
                                    implementationType: implementationType);
                            }
                        }
                        else
                        {
                            if (serviceType is null)
                            {
                                serviceCollection.AddKeyedTransient(
                                    serviceType: implementationType,
                                    serviceKey: attribute.ServiceKey);
                            }
                            else
                            {
                                serviceCollection.AddKeyedTransient(
                                    serviceType: serviceType,
                                    serviceKey: attribute.ServiceKey,
                                    implementationType: implementationType);
                            }
                        }

                        break;

                    default:
                        throw new ArgumentOutOfRangeException(
                            $"Attributed Service {implementationType.Name} is of an unsupported service lifetime.");
                }
            }
        }
    }
}
