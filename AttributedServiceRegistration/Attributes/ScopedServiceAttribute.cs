using System;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    public class ScopedServiceAttribute : ServiceBaseAttribute
    {
        /// <summary>
        /// Tag a service as a Scoped Service
        /// </summary>
        public ScopedServiceAttribute() : base(ServiceLifetime.Scoped)
        {
        }

        /// <summary>
        /// Tag a service as a Scoped Service with a Service Key
        /// </summary>
        /// <param name="serviceKey">Key to register the service with</param>
        public ScopedServiceAttribute(object serviceKey) : base(serviceKey, ServiceLifetime.Scoped)
        {
        }
    }
}
