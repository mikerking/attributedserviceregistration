using System;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Attributes
{

    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    public class TransientServiceAttribute : ServiceBaseAttribute
    {
        /// <summary>
        /// Tag a service class as a Transient Service
        /// </summary>
        public TransientServiceAttribute() : base(ServiceLifetime.Transient)
        {
        }

        /// <summary>
        /// Tag a service class as a Transient Service with a Service Key
        /// </summary>
        /// <param name="serviceKey">Key to register the service with</param>
        public TransientServiceAttribute(object serviceKey) : base(serviceKey, ServiceLifetime.Transient)
        {
        }
    }
}
