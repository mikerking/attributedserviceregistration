using System;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    public class SingletonServiceAttribute : ServiceBaseAttribute
    {
        /// <summary>
        /// Tag a service as a Singleton Service
        /// </summary>
        public SingletonServiceAttribute() : base(ServiceLifetime.Singleton)
        {
        }

        /// <summary>
        /// Tag a service as a Singleton Service with a Service Key
        /// </summary>
        /// <param name="serviceKey">Key to register the service with</param>
        public SingletonServiceAttribute(object serviceKey) : base(serviceKey, ServiceLifetime.Singleton)
        {
        }
    }
}
