using System;
using Microsoft.Extensions.DependencyInjection;

namespace AttributedServiceRegistration.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    public class ServiceBaseAttribute : Attribute
    {
        /// <summary>
        /// The service lifetime
        /// </summary>
        public ServiceLifetime Lifetime { get; private set; }

        /// <summary>
        /// Optional service key. Null to ignore
        /// </summary>
        public object ServiceKey { get; private set; }

        /// <summary>
        /// Construct service base attribute
        /// </summary>
        /// <param name="lifetime">The service lifetime</param>
        protected ServiceBaseAttribute(ServiceLifetime lifetime)
        {
            ServiceKey = null;
            Lifetime = lifetime;
        }

        /// <summary>
        /// Construct service base attribute with a service key
        /// </summary>
        /// <param name="serviceKey">The service key. Null if not to be used</param>
        /// <param name="lifetime">The service lifetime</param>
        protected ServiceBaseAttribute(object serviceKey, ServiceLifetime lifetime)
        {
            ServiceKey = serviceKey;
            Lifetime = lifetime;
        }
    }
}
